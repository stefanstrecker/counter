defmodule CounterWeb.HeroComponent do
  # If you generated an app with mix phx.new --live,
  # the line below would be: use MyAppWeb, :live_component
  use CounterWeb, :live_component

  def render(assigns) do
    ~H"""
    <div class="flex flex-col items-center justify-center text-4xl" phx-click="say_hello" phx-target={@myself}>
      <%= @content %>
    </div>
    """
  end

  def handle_event("say_hello", _params, socket) do
    {:noreply,
     socket
     |> assign(number: 99)}
  end
end

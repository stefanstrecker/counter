defmodule CounterWeb.MyComponents do
  use Phoenix.Component

  @doc """
  Function component. Renders a greeting.
  """
  #  attr :name, :string, required: true
  attr :name, :string, default: "Tom"

  def greet(assigns) do
    ~H"""
    <p>Hello, <%= @name %>!</p>
    """
  end

end

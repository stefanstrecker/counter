defmodule CounterWeb.PageLive do
  @moduledoc """
  See https://elixirprogrammer.com/learn/elixir-phoenix-liveview-counter-with-tailwind-css/
  """
  use CounterWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(number: 0)
     |> assign(sometitle: "Title")
     |> assign(somecontent: "Content")
     |> assign(content: "Some other heading")}
  end

  def handle_event("inc", _params, socket) do
    {:noreply,
     socket
     |> update(:number, &(&1 + 1))}
  end

  def handle_event("dec", _params, socket) do
    {:noreply,
     socket
     |> update(:number, &max(0, &1 - 1))}
  end

  def handle_event("clear", _params, socket) do
    {:noreply,
     socket
     |> assign(number: 0)}
  end
end
